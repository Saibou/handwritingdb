from django.core.management.base import BaseCommand

from main.models import QPartitioningScale, QPartitioning


# ОСТ дробное
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[1,2,3,4,5,6,7,25],
#                                                        partitioning_scale_name='Вариант Гусаковой')
#         for num, partscale in enumerate(partscales):
#             intscore = num % 6 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=intscore,
#                 interval_score_id=intscore
#             )

# САН
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[18, 19, 20],
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 6 + num % 6 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 6 + 1,
#                 interval_score_id=intscore
#             )
#

# ОЧХ стены
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[8,9,10,11,12,13,14,15,16,17],
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 38 + num % 9 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 9 + 1,
#                 interval_score_id=intscore
#             )


# ОЧХ сырые
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[9, 12],
#                                                        # partscales = QPartitioningScale.objects.filter(scale_id__in=[8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
#                                                        partitioning_scale_name=2,
#                                                        interval_set_id__in=(21, 22, 23, 24, 25, 26, 27, 28, 29, 30))
#         for num, partscale in enumerate(partscales):
#             intscore = 95 + num % 10 + 1
#             # print('partscale: ', partscale, "\nintscore: ", intscore)
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 10 + 1,
#                 interval_score_id=intscore
#             )

# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[11, 14],
#                                                        # partscales = QPartitioningScale.objects.filter(scale_id__in=[8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
#                                                        partitioning_scale_name=2,
#                                                        interval_set_id__in=(21, 22, 23, 24, 25, 26, 27, 28, 29, 30))
#         for num, partscale in enumerate(partscales):
#             intscore = 108 + num % 10 + 1
#             # print('partscale: ', partscale, "\nintscore: ", intscore)
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 10 + 1,
#                 interval_score_id=intscore
#             )

# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[13, 16, 17],
#                                                        # partscales = QPartitioningScale.objects.filter(scale_id__in=[8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
#                                                        partitioning_scale_name=2,
#                                                        interval_set_id__in=(21, 22, 23, 24, 25, 26, 27, 28, 29, 30))
#         for num, partscale in enumerate(partscales):
#             intscore = 118 + num + 1
#             # print('partscale: ', partscale, "\nintscore: ", intscore)
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 10 + 1,
#                 interval_score_id=intscore
#             )

# БП агрессия для мужчин
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id=22,
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 47 + num % 6 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 6 + 1,
#                 interval_score_id=intscore
#             )


# БП гнев для мужчин
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id=22,
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 53 + num % 6 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 6 + 1,
#                 interval_score_id=intscore
#             )


# БП враждебность для мужчин
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id=22,
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 59 + num % 6 + 1
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 6 + 1,
#                 interval_score_id=intscore
#             )

# БП для женщин
# class Command(BaseCommand):
#     def handle(self, *args, **options):
#         partscales = QPartitioningScale.objects.filter(scale_id__in=[21, 22, 23],
#                                                        partitioning_scale_name=2)
#         for num, partscale in enumerate(partscales):
#             intscore = 65 + num % 6 + 1  # неправильно. Берет только шесть записей
#             QPartitioning.objects.create(
#                 partitioning_scale_id=partscale.partitioning_scale_id,
#                 number_in_partitioning=num % 6 + 1,
#                 interval_score_id=intscore
#             )

# ОСТ стандартное
class Command(BaseCommand):
    def handle(self, *args, **options):
        partscales = QPartitioningScale.objects.filter(scale_id__in=[2,3,4,5,6,7,25],
                                                       partitioning_scale_name=3)
        for num, partscale in enumerate(partscales):
            intscore = 148 + num % 3 + 1
            QPartitioning.objects.create(
                partitioning_scale_id=partscale.partitioning_scale_id,
                number_in_partitioning=intscore,
                interval_score_id=intscore
            )