# Generated by Django 3.0.2 on 2020-05-16 15:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0019_auto_20200515_2330'),
    ]

    operations = [
        migrations.AddField(
            model_name='hwpartialsign',
            name='general_jsm_number',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='hwpartialsign',
            name='jsm_number',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='hwpartialsign',
            name='opt_general_jsm_number',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
