# Generated by Django 3.0.2 on 2020-05-09 17:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_hwgeneralsignvalue_jsm_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='person_number',
            field=models.PositiveIntegerField(blank=True, editable=False, null=True),
        ),
    ]
