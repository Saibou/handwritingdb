# Generated by Django 3.0.2 on 2020-03-12 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HwConcretization',
            fields=[
                ('concretization_id', models.AutoField(primary_key=True, serialize=False)),
                ('concretization', models.CharField(max_length=255)),
                ('concretization_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_concretization',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwElement',
            fields=[
                ('element_id', models.AutoField(primary_key=True, serialize=False)),
                ('element', models.CharField(blank=True, max_length=255, null=True)),
                ('element_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_element',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwGeneralSignType',
            fields=[
                ('general_sign_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('general_sign_type', models.CharField(blank=True, max_length=255, null=True)),
                ('general_sign_type_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_general_sign_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwGeneralSignValue',
            fields=[
                ('general_sign_value_id', models.AutoField(primary_key=True, serialize=False)),
                ('general_sign_value', models.CharField(blank=True, max_length=255, null=True)),
                ('general_sign_value_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_general_sign_value',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwGeneralWriting',
            fields=[
                ('general_writing_id', models.AutoField(primary_key=True, serialize=False)),
                ('comment', models.TextField(blank=True, null=True)),
                ('amount_sign', models.IntegerField(blank=True, null=True)),
                ('amount_all', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_general_writing',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwImage',
            fields=[
                ('image_id', models.AutoField(primary_key=True, serialize=False)),
                ('image_path', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'hw_image',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwLetter',
            fields=[
                ('letter_id', models.AutoField(primary_key=True, serialize=False)),
                ('letter', models.CharField(max_length=255)),
                ('letter_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_letter',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwLetterConcretization',
            fields=[
                ('letter_concretization_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'hw_letter_concretization',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwLetterElement',
            fields=[
                ('letter_element_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'hw_letter_element',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwLinkNext',
            fields=[
                ('link_next_id', models.AutoField(primary_key=True, serialize=False)),
                ('link_next', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'hw_link_next',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwPartialSign',
            fields=[
                ('partial_sign_id', models.AutoField(primary_key=True, serialize=False)),
                ('relation_between_concretizations', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_partial_sign',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwPartialSignType',
            fields=[
                ('partial_sign_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('partial_sign_type', models.CharField(max_length=255)),
                ('partial_sign_type_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_partial_sign_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwPartialSignValue',
            fields=[
                ('partial_sign_value_id', models.AutoField(primary_key=True, serialize=False)),
                ('partial_sign_value', models.CharField(blank=True, max_length=255, null=True)),
                ('partial_sign_value_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_partial_sign_value',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwPartialSignValueConcretization',
            fields=[
                ('partial_sign_value_concretization_id', models.AutoField(primary_key=True, serialize=False)),
                ('partial_sign_value_concretization', models.CharField(blank=True, max_length=255, null=True)),
                ('partial_sign_value_concretization_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_partial_sign_value_concretization',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwPartialWriting',
            fields=[
                ('partial_writing_id', models.AutoField(primary_key=True, serialize=False)),
                ('amount_sign', models.IntegerField(blank=True, null=True)),
                ('amount_all', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_partial_writing',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwSet',
            fields=[
                ('set_id', models.IntegerField(primary_key=True, serialize=False)),
                ('amount', models.IntegerField(blank=True, null=True)),
                ('verity', models.IntegerField(blank=True, null=True)),
                ('test_number', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'hw_set',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwTranscription',
            fields=[
                ('transcription_id', models.AutoField(primary_key=True, serialize=False)),
                ('letter_place', models.IntegerField(blank=True, null=True)),
                ('variant', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'hw_transcription',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HwType',
            fields=[
                ('type_id', models.AutoField(primary_key=True, serialize=False)),
                ('type', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'hw_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('person_id', models.AutoField(primary_key=True, serialize=False)),
                ('person_name', models.CharField(blank=True, max_length=255, null=True)),
                ('person_surname', models.CharField(blank=True, max_length=255, null=True)),
                ('person_patronymic', models.CharField(blank=True, max_length=255, null=True)),
                ('birthday', models.DateTimeField(blank=True, null=True)),
                ('sex', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'db_table': 'person',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('position_id', models.AutoField(primary_key=True, serialize=False)),
                ('position', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'position',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QAnswer',
            fields=[
                ('answer_id', models.AutoField(primary_key=True, serialize=False)),
                ('number_in_question', models.IntegerField(blank=True, null=True)),
                ('score', models.IntegerField(blank=True, null=True)),
                ('question_id_id', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_answer',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QAnswerText',
            fields=[
                ('answer_text_id', models.AutoField(primary_key=True, serialize=False)),
                ('answer_text', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_answer_text',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QComment',
            fields=[
                ('comment_id', models.AutoField(primary_key=True, serialize=False)),
                ('comment_text', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_comment',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QIntervalName',
            fields=[
                ('interval_name_id', models.AutoField(primary_key=True, serialize=False)),
                ('interval_nickname', models.CharField(blank=True, max_length=255, null=True)),
                ('interval_name', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_interval_name',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QIntervalScore',
            fields=[
                ('interval_score_id', models.AutoField(primary_key=True, serialize=False)),
                ('min_score', models.IntegerField(blank=True, null=True)),
                ('max_score', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_interval_score',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QIntervalSet',
            fields=[
                ('interval_set_id', models.AutoField(primary_key=True, serialize=False)),
                ('interval_name_id', models.IntegerField(blank=True, null=True)),
                ('number_in_interval_set', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_interval_set',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QIntervalSetName',
            fields=[
                ('interval_set_name_id', models.AutoField(primary_key=True, serialize=False)),
                ('interval_set_name', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_interval_set_name',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QPartitioning',
            fields=[
                ('partitioning_id', models.AutoField(primary_key=True, serialize=False)),
                ('interval_id', models.IntegerField(blank=True, null=True)),
                ('number_in_partitioning', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_partitioning',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QPartitioningScale',
            fields=[
                ('partitioning_scale_id', models.AutoField(primary_key=True, serialize=False)),
                ('partitioning_scale_name', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_partitioning_scale',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QPersonSelection',
            fields=[
                ('person_selection_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'q_person_selection',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QQuestion',
            fields=[
                ('question_id', models.AutoField(primary_key=True, serialize=False)),
                ('number_in_questionnaire', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_question',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QQuestionnaire',
            fields=[
                ('questionnaire_id', models.AutoField(primary_key=True, serialize=False)),
                ('questionnaire_nickname', models.CharField(blank=True, max_length=255, null=True)),
                ('questionnaire_name', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_questionnaire',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QQuestionScale',
            fields=[
                ('question_scale_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'q_question_scale',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QQuestionText',
            fields=[
                ('question_text_id', models.AutoField(primary_key=True, serialize=False)),
                ('question_text', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_question_text',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QResult',
            fields=[
                ('result_id', models.AutoField(primary_key=True, serialize=False)),
                ('raw_score', models.IntegerField(blank=True, null=True)),
                ('result_score', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'q_result',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='QScale',
            fields=[
                ('scale_id', models.AutoField(primary_key=True, serialize=False)),
                ('scale_name', models.CharField(blank=True, max_length=255, null=True)),
                ('scale_nickname', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'q_scale',
                'managed': False,
            },
        ),
    ]
