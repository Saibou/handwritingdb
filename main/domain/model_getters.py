from main.models import QPartitioning, QQuestion, QQuestionScale, QResult, \
    QScale


def get_assessments_by_questionnaire(questionnaire_id, partitioning_scale_name_id, scales=None):
    if scales is None:
        questions = QQuestion.objects.filter(
            questionnaire_id=int(questionnaire_id)).values_list('question_id',
                                                                flat=True)
        scales = QQuestionScale.objects.prefetch_related('scale')\
            .filter(question_id__in=questions).values_list('scale_id', flat=True)
    return QPartitioning.objects\
        .select_related('interval_score')\
        .select_related('partitioning_scale__interval_set__interval_name')\
        .filter(
           partitioning_scale__partitioning_scale_name_id=partitioning_scale_name_id,
           partitioning_scale__scale_id__in=scales)


def get_results_by_person(person_id, scales):
    return QResult.objects\
        .prefetch_related('scale')\
        .filter(person_id=int(person_id), scale_id__in=scales)


def get_results_by_scale(scale_id):
    return QResult.objects.prefetch_related('scale').filter(scale_id=scale_id)


def get_results_by_questionnaire(questionnaire_id, scales=None):
    if scales is None:
        scales = get_scales_by_questionnaire(questionnaire_id)
    return QResult.objects.select_related('scale', 'person').filter(scale__in=scales)


def get_scales_by_questionnaire(questionnaire_id):
    sc = QQuestionScale.objects \
        .filter(question__questionnaire=questionnaire_id) \
        .values('scale_id').exclude(scale_id=24)
    return QScale.objects.filter(scale_id__in=sc)