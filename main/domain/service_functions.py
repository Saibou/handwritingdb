from django.db.models import Q

from main.domain.jsm_func import divide_examples
from main.domain.model_getters import get_assessments_by_questionnaire
from main.models import Person, HwSet, HwTranscription, HwPartialWriting, \
    QResult, QQuestionScale, HwGeneralWriting


# функция разделения примеров на + и - по полу
def divide_by_sex(example):
    if example[1].sex == 'ж':
        return True
    elif example[1].sex == 'м':
        return False


# функция разделения примеров на + и - по полу
def divide_by_result(example, res_plus, res_minus):

    if example[1].person_id in res_plus:
        return True
    elif example[1].person_id in res_minus:
        return False


# подготовить примеры вида [{оценка, шкала}, респондент] для  дальнейшей ДСМ-обработки
def prepare_examples_q(results, questionnaire_id, partitioning_scale_name_id=2, assessments=None, split_func=divide_by_sex, **div_params):

    if assessments is None:
        assessments = get_assessments_by_questionnaire(questionnaire_id, partitioning_scale_name_id)

    ordered_results = results.order_by('person_id')

    examples = []
    effect_set = set()
    current_resp = None
    cached_assessments = {}

    for result in ordered_results:
        if current_resp != result.person:
            effect_set = set()
        current_resp = result.person
        assessments_params = (assessments, questionnaire_id, result)
        result_params = (result.scale_id, result.result_score)
        assessment = cached_assessments.get(result_params)
        if not assessment:
            assessment = count_assessment(*assessments_params)
            cached_assessments[result_params] = assessment
        effect = (assessment.partitioning_scale.interval_set.interval_name.interval_nickname,
                  result.scale.scale_nickname)
        effect_set.add(effect)
        if [effect_set, current_resp] not in examples:
            examples.append([effect_set, current_resp])

    examples_plus, examples_minus = divide_examples(examples, split_func=split_func, **div_params)

    return examples_plus, examples_minus


# общие признаки подписи
def get_general_features(effect_set, gw_features, uninformative_features):
    for g in gw_features:
        feature = g.general_sign_value.jsm_number
        if feature and feature not in uninformative_features:
            effect_set.add(g.general_sign_value.get_general_value())
    return effect_set


# обобщенные частные признаки подписи
def get_gen_partial_features(effect_set, pw_features):
    for p in pw_features:
        if p.partial_sign.general_jsm_number:
            if p.partial_sign.partial_sign_value.partial_sign_type.partial_sign_type_number == 1:
                effect_set.add(p.get_partial_value())
            else:
                effect_set.add(p.partial_sign.get_gen_partial_value())
    return effect_set


# общие признаки подписи по группам
def get_general_group_features(effect_set, gw_features, sign_group_types):
    for g in gw_features:
        feature = g.general_sign_value.jsm_number
        type = g.general_sign_value.general_sign_type.general_sign_type_number
        if type in sign_group_types and feature:
            effect_set.add(g.general_sign_value.get_general_value())
    return effect_set


# частные признаки подписи
def get_partial_features(effect_set, pw_features):
    for p in pw_features:
        if p.partial_sign:
            effect_set.add(p.partial_sign.get_partial_value())
    return effect_set


def prepare_examples_hw(ordered_sets, split_func, sim_signs, **div_params):

    uninformative_features = ['1;1', '2;2', '9;1', '11;1', '12;1']
    sign_group_types = [3, 4, 5, 13, 14]

    gw = HwGeneralWriting.objects \
        .select_related('set', 'general_sign_value', 'general_sign_value__general_sign_type', ) \
        .distinct()

    pw = HwPartialWriting.objects\
        .select_related('first_letter_transcription', 'partial_sign__partial_sign_value__partial_sign_type',
                        'partial_sign__first_letter_concretization__letter_element__general_element',
                        'partial_sign__second_letter_concretization__letter_element__general_element',
                        'partial_sign__partial_sign_value_concretization')

    examples = []

    for hw_set in ordered_sets:
        gw_features = gw.filter(set=hw_set)
        pw_features = pw.filter(first_letter_transcription__set=hw_set)
        effect_set = set()

        if sim_signs == 'gen_elem':
            # общие признаки подписи
            effect_set | get_general_features(effect_set, gw_features, uninformative_features)

            # обобщенные частные признаки подписи
            effect_set | get_gen_partial_features(effect_set, pw_features)

        elif sim_signs == 'groups':
            # общие признаки подписи по группам
            effect_set | get_general_group_features(effect_set, gw_features, sign_group_types)

            effect_set | get_partial_features(effect_set, pw_features)

        examples.append([effect_set, hw_set.person])

    examples_plus, examples_minus = divide_examples(examples, split_func, **div_params)

    return examples_plus, examples_minus


# Подсчет оценки за опросник в общем виде
def count_assessment(assessments, questionnaire_id, result):

    kwargs = dict(partitioning_scale__scale=result.scale,
                  interval_score__min_score__lte=result.result_score,
                  interval_score__max_score__gte=result.result_score)

    # в опроснике Басса-Перри оценка зависит от пола респондента
    if int(questionnaire_id) == 4:
        if result.person.sex == 'м':
            kwargs['partitioning_id__range'] = (875, 892)
        elif result.person.sex == 'ж':
            kwargs['partitioning_id__range'] = (893, 910)

    # в опроснике ОЧХ сырые баллы переводятся в "стены" при заполнении теста,
    # поэтому здесь уточняется, что interval_set_name=3 (нн-вв-9)
    elif int(questionnaire_id) == 2:
        kwargs['partitioning_scale__interval_set__interval_set_name'] = 3
    # в остальных случаях просто берем ту оценку, в интервал которой попадает
    # наш result_score

    assessment = assessments.get(**kwargs)

    return assessment


# функция подсчета людей заполненными опросниками && подписями
def count_persons_w_tests_n_hw(questionnaire_id):
    scales = QQuestionScale.objects.filter(question__questionnaire=questionnaire_id).values_list('scale_id', flat=True).distinct()

    # результаты по скольки шкалам есть у респондента (group by person)
    # results = QResult.objects.filter(scale__in=scales).values('person').annotate(total=Count('person_id'))

    # находим респондентов, заполнивших нужный опросник
    respondents_q = QResult.objects.filter(scale__in=scales).values_list('person_id').distinct()

    # находим респондентов с частными признаками подписи
    first_letter_transcriptions = HwPartialWriting.objects.all().values_list('first_letter_transcription_id', flat=True)
    second_letter_transcriptions = HwPartialWriting.objects.all().values_list('second_letter_transcription_id', flat=True)

    sets = HwTranscription.objects.filter(Q(transcription_id__in=first_letter_transcriptions) | Q(
        transcription_id__in=second_letter_transcriptions)).values_list('set_id')

    respondents_hw = HwSet.objects.filter(set_id__in=sets).values_list('person_id', flat=True).distinct()

    # находим пересечение респондентов
    person_list = Person.objects.filter(Q(person_id__in=respondents_q) & Q(person_id__in=respondents_hw)).order_by('person_surname')
    # person_list_q = Person.objects.filter(person_id__in=respondents_q).order_by('person_surname')
    # person_list_hw = Person.objects.filter(person_id__in=respondents_hw).order_by('person_surname')

    return person_list
